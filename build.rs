#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]

#![allow(non_snake_case)]
extern crate build_helper;
extern crate itertools;
extern crate byteorder;
extern crate network;
extern crate parser;

use network::*;
use parser::*;

use std::f64;
use itertools::Itertools;
use std::fs::File;

use build_helper::Endianness::*;
use byteorder::{BigEndian, LittleEndian, WriteBytesExt};

/// Constants!
const EPOCHS: usize = 1_000;
const  BATCH: usize =    32;
const   RATE: f64   =   0.5;

pub fn main() {

  // We begin by loading the training images.
  let train_image : Box<[Matrix<f64,U1,U784>]> =
    images(include_bytes!("./data/train-images.idx3-ubyte"))
      .to_full_result().map(|(n, r, c, i)|
        { assert!(n == 60_000);
          assert!(r ==     28);
          assert!(c ==     28);
          unsafe{transmute(i)}
        }).unwrap();

  // Load the training labels
  let train_label =
    labels(include_bytes!("./data/train-labels.idx1-ubyte"))
      .to_full_result().map(|(n, i)|
        { assert!(n == 60_000);
          i
        }).unwrap();

  // Zip 'em together
  let training =
    train_image.iter().zip(train_label.iter().cloned()).collect_vec();

  // ...and train a network.
  let (weight, bias) : (Matrix<f64, U784, U10>, Matrix<f64, U1, U10>) =
    train(
      RATE,
      EPOCHS,
      BATCH,
      &training[..]);

  // We encode the resulting parameters into files that can be loaded
  // at compile-time by the software making use of this network. In
  // this case, that's just `main.rs`.

  // We begin by choosing a path for each type of parameter...
  let weight_path = build_helper::out_dir().join("weight.bin");
  let bias_path   = build_helper::out_dir().join("bias.bin");

  // ...and seize file handles to those paths.
  let mut weight_file = File::create(&weight_path).expect(
    &format!("Could not create {}", &weight_path.display()));
  let mut bias_file = File::create(&bias_path).expect(
    &format!("Could not create {}", &bias_path.display()));

  // The parameters are float arrays, but Rust only gives us an
  // `include_bytes!` macro---not `include_floats!`. In `main.rs`,
  // we will simply type-pun (via `std::mem::transmute`) these
  // included byte arrays to nalgebra float `Matrix` types.
  // Since an `f64` spans eight bytes, we must take care to respect
  // the target endianness when serializing the byte arrays.
  let endian =
    build_helper::target::endian()
      .expect("Could not detect target endian.");

  // Finally, we write each float of each parameter to the
  // appropriate file:
  let weight = weight.data.iter();
  let bias   = bias.data.iter();
  match endian {
    Big    => {weight.for_each(|&d| weight_file.write_f64::<BigEndian>(d).unwrap());
                 bias.for_each(|&d|   bias_file.write_f64::<BigEndian>(d).unwrap())},
    Little => {weight.for_each(|&d| weight_file.write_f64::<LittleEndian>(d).unwrap());
                 bias.for_each(|&d|   bias_file.write_f64::<LittleEndian>(d).unwrap())}
  }
}