# Handwriting Recognition

A showcase of nifty implementation techniques for Rust projects.

## Overview

This repository provides a library that recognizes handwritten digits from the MNIST dataset using a one-layer feed-forward neural network. This is _not_ a state-of-the-art recognizer and the purpose of this repository is _not_ to be a one-stop-shop for your digit-recognition needs. Rather, this is a simple showcase of a number of useful implementation techniques:

 - constructing parsers with [nom](http://rust.unhandledexpression.com/nom/)
 - including non-code assets in Rust projects
 - fast, type-checked linear algebra with [nalgebra](http://nalgebra.org/)
 - compile-time neural network training

## Organization

The top-level of this repository is a [library](./src/lib.rs) providing a `recognize` function that consumes a 28×28 image of a handwritten character and produces a number (the digit it thinks the image represents). This neural network is trained at compile-time by a [build script](./build.rs). The [data](./data) directory contains both the training and testing data for this network.

To support code-sharing between the compile-time and run-time phases of this library, this repository is further split into two sub-crates: [parser](./parser) and [network](./network).

The [parser](./parser) crate uses [nom](http://rust.unhandledexpression.com/nom/) to parse the MNIST digit datasets. The [build script](./build.rs) uses this crate to interpret the training data. The [run-time phase of the library](./src/lib.rs) uses this crate to load testing data for both a benchmark and a test evaluating the network's accuracy.

The [network](./network) crate uses [nalgebra](http://nalgebra.org/) to implement a one-layer feed-forward neural network. The [build script](./build.rs) uses this crate to train a neural network. The [run-time phase of the library](./src/lib.rs) uses the parameters trained during compile-time and the [network](./network) crate to recognize digits.