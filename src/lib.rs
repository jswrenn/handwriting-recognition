#![feature(test)]

#![allow(non_snake_case)]

extern crate network;
#[cfg(test)] #[macro_use] extern crate lazy_static;
#[cfg(test)] extern crate test;
#[cfg(test)] extern crate parser;

use std::f64;
use network::*;
#[cfg(test)] use parser::*;

/// `recognize` consumes a 1×784 matrix representing an image of a
/// hand-written digit, and produces a label (a number ranging from 0
/// to 9)
pub fn recognize(input: &Matrix<f64, U1, U784>) -> usize
{
  static RAW_WEIGHT : &'static [u8; 62_720]
    = include_bytes!(concat!(env!("OUT_DIR"), "/weight.bin"));

  static RAW_BIAS   : &'static [u8; 80]
    = include_bytes!(concat!(env!("OUT_DIR"), "/bias.bin"));

  let WEIGHT: &Matrix<f64, U784, U10>
    = unsafe{std::mem::transmute(RAW_WEIGHT)};

  let BIAS  : &Matrix<f64, U1, U10>
    = unsafe{std::mem::transmute(RAW_BIAS)};

  network::recognize(input, WEIGHT, BIAS)
}

#[cfg(test)]
mod tests {
  use ::*;

  lazy_static! {
    static ref TEST_IMAGES : Box<[Matrix<f64,U1,U784>]> =
      images(include_bytes!("../data/t10k-images.idx3-ubyte"))
        .to_full_result().map(|(n, r, c, i)|
          { assert!(n == 10_000);
            assert!(r ==     28);
            assert!(c ==     28);
            unsafe{transmute(i)}
          }).unwrap();

    static ref TEST_LABELS : Box<[u8]> =
      labels(include_bytes!("../data/t10k-labels.idx1-ubyte"))
        .to_full_result().map(|(n, i)|
          { assert!(n == 10_000);
            i
          }).unwrap();
  }

  /// Evaluate:
  /// Consumes:
  ///   •  [1×784] images 
  ///   •  [label] labels 
  ///   • 784×10   weights
  ///   •   1×10   bias
  /// Produces:
  ///   • %of correctly recognized images 
  fn evaluate
      ( input   : &[(u8,Matrix<f64,   U1, U784>)]
      )        -> f64
  {
    (input.iter().filter(|&&(label, image)|
      recognize(&image) == label as usize)
        .count() as f64) / (input.len() as f64)
  }

  /// Test the accuracy of the recognizer on a withheld dataset of
  /// 10,000 labeled images.
  ///
  /// Although there is some variance between builds, the network's
  /// accuracy should be about 90%, and certainly not lower than 85%.
  #[test]
  fn accuracy() {
    let test_input =
      (*TEST_LABELS).iter().cloned().zip((*TEST_IMAGES).iter().cloned())
        .collect::<Vec<_>>();

    assert!(evaluate(&test_input[..]) > 0.85);
  }

  /// Benchmark of the recognizer on recognizing each image in the
  /// withheld dataset of 10,000 labeled images.
  #[bench]
  fn bench(b: &mut test::Bencher) {
    b.iter(||
      for image in &TEST_IMAGES[..] {
        test::black_box(recognize(image));
      });
  }
}
