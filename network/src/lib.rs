//! The `network` crate implements a one-layer feed-forward neural
//! network.

extern crate typenum;
extern crate nalgebra;
extern crate rulinalg;
extern crate alga;
extern crate rand;

#[doc(no_inline)]
pub use nalgebra::MatrixNM as Matrix;

/// A type-level representation of the number 1.
#[doc(no_inline)] pub use nalgebra::U1;

/// A type-level representation of the number 10.
#[doc(no_inline)] pub use nalgebra::U10;

/// A type-level representation of the number 784.
#[doc(no_inline)] pub use typenum::U784;


/// The forward pass consumes a training image and the network's
/// parameters, and produces a vector of logits.
///
/// Its arguments are
///
///   - a   1×784 matrix representing an image,
///   - a 784×10  matrix representing the network's weights,
///   - a   1×10  matrix representing the network's bias,
///
/// …and it produces
///
///   - a 1×10    matrix of logits.
pub fn forward
    ( image   : &Matrix<f64,   U1, U784>
    , weights : &Matrix<f64, U784,  U10>
    , bias    : &Matrix<f64,   U1,  U10>
    )         -> Matrix<f64,   U1,  U10>
{
  (image * weights) + bias
}


/// The backward pass consumes the logits, a learning rate and the
/// weight parameters of the network, and produces adjustments to
/// make to the weight and bias parameters to improve the performance
/// of the network.
///
/// Its arguments are
///
///   - a scalar learning rate,
///   - a 1×784 matrix representing the network's weights,
///   - a 1×10  matrix representing the logits produced by the [forward pass](fn.forward.html),
///
/// …and it produces
///
///   - a 784×10 matrix representing the appropriate adjustment to the network's weight
///   - a   1×10 matrix representing the appropriate adjustment to the network's bias
pub fn backward
    ( rate    :  f64
    , image   : &Matrix<f64,   U1, U784>
    , softmax : &Matrix<f64,   U1,  U10>
    , onehot  : &Matrix<f64,   U1,  U10>
    )        -> (Matrix<f64, U784,  U10>
                ,Matrix<f64,   U1,  U10>)
{
  let delta_bias    = (softmax - onehot) * -rate;
  let delta_weight  = image.transpose()  * delta_bias;
  (delta_weight, delta_bias)
}


/// The [`softmax`](https://en.wikipedia.org/wiki/Softmax_function)
/// function consumes a vector of logits that are arbitrary real
/// numbers, and normalizes them such that they sum to 1.
///
/// Its arguments are 
///
///   - a 1×10 matrix of logits
///
/// …and it produces
///
///   - a 1×10 matrix normalized logits.
pub fn softmax
    ( logits  : &Matrix<f64,   U1,  U10> )
             ->  Matrix<f64,   U1,  U10>
{
  use alga::general::Real;

  let num = logits.map(Real::exp);
  num / num.iter().sum::<f64>()
}


/// An epoch is a single round of training of the neural network. It
/// consumes a learning rate, training data, and network parameters,
/// and produces adjustments to those parameters to improve the
/// performance of the network.
///
///
/// Its arguments are
///   - a scalar learning rate,
///   - a collection of 1×784 matrices representing training images,
///   - a collection of sample labels,
///   - a 784×10 matrix representing the network's weights,
///   - a 1×10 matrix representing the network's bias,
///
/// …and it produces
///
///   - a 784×10 matrix representing the appropriate adjustment to the network's weight
///   - a   1×10 matrix representing the appropriate adjustment to the network's bias
pub fn epoch
    ( rate    : f64
    , sample  : &[&(&Matrix<f64,   U1, U784>, u8)]
    , weights :      Matrix<f64, U784,  U10>
    , bias    :      Matrix<f64,   U1,  U10>
    )          ->   (Matrix<f64, U784,  U10>,
                     Matrix<f64,   U1,  U10>)
{
  use nalgebra::zero;

  #[allow(non_snake_case)]
  let HOT: [Matrix<f64, U1, U10>; 10] =
   [[1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,].into(),
    [0.,1.,0.,0.,0.,0.,0.,0.,0.,0.,].into(),
    [0.,0.,1.,0.,0.,0.,0.,0.,0.,0.,].into(),
    [0.,0.,0.,1.,0.,0.,0.,0.,0.,0.,].into(),
    [0.,0.,0.,0.,1.,0.,0.,0.,0.,0.,].into(),
    [0.,0.,0.,0.,0.,1.,0.,0.,0.,0.,].into(),
    [0.,0.,0.,0.,0.,0.,1.,0.,0.,0.,].into(),
    [0.,0.,0.,0.,0.,0.,0.,1.,0.,0.,].into(),
    [0.,0.,0.,0.,0.,0.,0.,0.,1.,0.,].into(),
    [0.,0.,0.,0.,0.,0.,0.,0.,0.,1.,].into()];

  sample.iter().map(|&&(image, label)|
    { let logits = forward(image, &weights, &bias);
      let lngits = softmax(&logits);
      backward(rate, image, &lngits, &HOT[label as usize])
    })
  .fold((zero(), zero()),
    |(w, b), (x, c)| (w + x, b + c))
}


/// The training function consumes a learning rate, the number of
/// epochs that training is divided into, and the number of items
/// from training data to sample for each epoch, and it produces
/// adjustments to those parameters that improve the performance of
/// the network.
///
/// Its arguments are
///
///   - a scalar learning rate,
///   - a number of epochs,
///   - the number of training data to sample in eachepoch,
///   - a collection of 784×10 matrix and label pairs representing the network's weights,
///   - a 1×10 matrix representing the network's bias,
///
/// …and it produces
///
///   - a 784×10 matrix representing the network's weight parameters
///   - a   1×10 matrix representing the network's bias parameters
pub fn train
    ( rate    :   f64
    , epochs  :   usize
    , batch   :   usize
    , input   : &[(&Matrix<f64,   U1, U784>,u8)]
    )          ->  (Matrix<f64, U784,  U10>,
                    Matrix<f64,   U1,  U10>)
{
  use rand::{thread_rng, sample};

  // Initialize the rng for sampling from the training data.
  let mut rng = thread_rng();

  // Initialize the bias parameters
  let mut b: Matrix<f64,   U1, U10> =
    Matrix::from_element(0.);

  // Initialize the weights parameters
  let mut w: Matrix<f64, U784, U10> =
    Matrix::from_element(0.);

  // For each epoch...
  for _ in 0..epochs {
    // Sample `batch` images and labels from the training data.
    let sample = sample(&mut rng, input.iter(), batch);

    // Train on this sample
    let (weight, bias) =  epoch(rate, sample.as_slice(), w, b);

    // Update the weight and bias accordingly.
    w += weight / (batch as f64);
    b += bias   / (batch as f64);
  }

  (w, b)
}


/// The recognition function consumes an image of a hand-written
/// character and the network parameters, and produces a number from
/// 0 to 9 reflecting what digit the network believes the image
/// depicts.
///
/// Its arguments are
///
///   - a   1×784 matrix representing an image,
///   - a 784×10  matrix representing the network's weights,
///   - a   1×10  matrix representing the network's bias,
///
/// …and it produces
///
///   - a number ranging from 0 to 9 reflecting what digit the
///     network believes the image depicts.
pub fn recognize
    ( input   :  &Matrix<f64,   U1, U784>
    , weights :  &Matrix<f64, U784,  U10>
    , bias    :  &Matrix<f64,   U1,  U10>
    )        -> usize
{
  use rulinalg::utils::argmax;

  argmax(forward(input, weights, bias).as_slice()).0
}