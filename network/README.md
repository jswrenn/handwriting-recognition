# network

The [network](./) crate uses [nalgebra](http://nalgebra.org/) to implement a one-layer feed-forward neural network. The [build script](../build.rs) uses this crate to train a neural network. The [run-time phase of the library](../src/lib.rs) uses the parameters trained during compile-time and the [network](./) crate to recognize digits.


