//! A parsing library for the MNIST handwriting datasets.

#[macro_use]
extern crate nom;
use nom::*;

/// Parse label files.
named!(pub labels<&[u8], (u32, Box<[u8]>)>,
  do_parse!(verify!(be_u32, |v| v == 2049)
        >>  n:be_u32
        >>  l:count!(be_u8, n as usize)
        >> (n, l.into_boxed_slice())));

/// Parse image files.
named!(pub images<&[u8], (u32, u32, u32, Box<[f64]>)>,
  do_parse!(verify!(be_u32, |v| v == 2051)
        >>  n:be_u32
        >>  r:be_u32
        >>  c:be_u32
        >>  i:count!(
                map!(be_u8, |p| f64::from(p) / 255.0),
                        (r * c * n) as usize)
        >>  (n, r, c, i.into_boxed_slice())));


pub unsafe fn transmute<I, O>(i : Box<[I]>) -> Box<[O]> {
    use std::mem::{transmute, size_of};
    use std::slice::from_raw_parts_mut;
    let len : usize    = i.len();
    let ptr : *mut [I] = Box::into_raw(i);
    let ptr : &mut [I] = &mut *ptr;
    let ptr : *mut I   = ptr.as_mut_ptr();
    let ptr : *mut O   = transmute::<*mut I, *mut O>(ptr);
    let ptr : &mut [O] = from_raw_parts_mut(ptr, (size_of::<I>() * len) / size_of::<O>());
    Box::from_raw(ptr)
}