# parser

The [parser](./) crate uses [nom](http://rust.unhandledexpression.com/nom/) to parse the MNIST digit datasets. The [build script](../build.rs) uses this crate to interpret the training data. The [run-time phase of the library](../src/lib.rs) uses this crate to load testing data for both a benchmark and a test evaluating the network's accuracy.
